# Final Exam

Before you begin, check your current weighted total in class and your
theoretical maximum score. If your current weighted total is a 90 or greater,
congratulations, you do not need to take the final, you have earned an A. If
your theoretical maximum score is less than a 60, you may consider not taking
this exam as there is no rounding that I would perform to push you to a passing
grade. Keep in mind your theoretical maximum score assumes a perfect score on
the bonus assignment and final.

Complete all questions in their entirety. This exam is to be submitted via
GitLab pull request as usual by Monday, May 2, at 12:00 PM (noon). I will take
this exam one day late for a 10 point **penalty**.

## Questions (50 points)

This section as a whole is worth 40 points. Each question has its value noted
next to it.

### Git (15 points)

1. Explain, visually, what happens to the below image when a feat1 is rebased
   on master, and when master is merged into feat1. (12 points)
   ![GitRebaseMergeBase.png](GitRebaseMergeBase.png)
1. What is the risk of running `git push -f`? (3 points)

### Docker (12 points)

1. Explain the difference between ARG and ENV. (3 points)
1. When running containers, why would you want to run attached? (3 points)
1. When running containers, why would you want to use a composefile? (3 points)
1. What command(s) would you need to use images in a private container registry
   such as GitLab's repository container registry? (3 points)

### CI/CD Questions (6 points)

1. Explain the difference between a job, stage, and pipeline. (3 points)
1. If a pipeline has a variable defined in the CI YAML, in the project settings,
   and in the run arguments, which value is used? (3 points)

### Kubernetes Questions (17 points)

1. What is the difference between a deployment and a job? (3 points)
1. What is the purpose of a load balancer? (3 points)
1. Where can environment variables be sourced from besides literals? (3 points)
1. List three types of volumes that can be mounted to a pod. (3 points)
1. List three benefits of using a Kustomization YAML. (5 points)

## Implementation (50 points)

Use the code in this directory to accomplish all of the following.

- [ ] Write a Dockerfile capable of building a container image to host this
      page. (10 points)
- [ ] Write a GitLab CI pipeline that will build the site (`npm install` and
      `npm run build`), run tests (`npm test`) and build this Dockerfile hosting
      the image in the GitLab container registry. Make sure to utilize the cache
      to avoid redownloading the `node_modules`. (15 points)
- [ ] Provide Kubernetes manifests needed to host three replicas of this page that
      are accessible through ingress using the image from GitLab. (25 points)

Note: Sourcing the image from GitLab should be the hardest part of this section. If
you are unable to perform this task, using the local container registry is fine, but
will cause a loss of 5 points.

Hint: Use the files in this repository to identify an appropriate base image for your
Dockerfile and in the pipeline.


# Original README


Let's Encrypt Website
=====================

This is the repository for the main [Let's Encrypt website].

This site is built with [Hugo]. It's entirely static, no server-side code/scripting.

To see your changes, [install Hugo Extended], then run it with:

```sh
hugo server -F
```

And open [http://localhost:1313/] in your browser. Note that the `-F` flag will
show items to be published in the future (like blog posts with dates in the
future).

If you update javascript, css or layouts, you can run tests with:

```sh
npm install && npm run build && npm test
```

Contributions welcome.

# Troubleshooting with Hugo

If you see the error:

> Failed to load translations in file "en.toml": unsupported file extension .toml

Your version of Hugo is probably too old. Please use the version specified in [netlify.toml]

# Translations - internationalization (i18n)

To help with translation, please see [TRANSLATION.md].

[Let's Encrypt website]: https://letsencrypt.org/
[Hugo]: https://gohugo.io/
[install Hugo Extended]: https://gohugo.io/getting-started/installing
[http://localhost:1313/]: http://localhost:1313/
[TRANSLATION.md]: https://github.com/letsencrypt/website/blob/master/TRANSLATION.md
[netlify.toml]: https://github.com/letsencrypt/website/blob/master/netlify.toml
